# Définir les variables
$gitlabUrl = "https://gitlab.com/" # URL de votre instance GitLab
$projectId = 12345 # ID du projet GitLab
$minimumJiraCommits = 3 # Nombre minimum de commits Jira requis
$smtpServer = "smtp.example.com" # Serveur SMTP pour l'envoi d'e-mails
$smtpPort = 587 # Port SMTP
$smtpUsername = "youremail@example.com" # Nom d'utilisateur SMTP
$smtpPassword = "yourpassword" # Mot de passe SMTP
$fromEmailAddress = "gitlab-mr-notifier@example.com" # Adresse e-mail de l'expéditeur

# Obtenir un jeton d'accès personnel GitLab
$personalAccessToken = Get-GitLabPAT

# Récupérer les MR ouvertes
$openMers = Get-GitLabMergeRequest -Url $gitlabUrl -ProjectId $projectId -AccessToken $personalAccessToken

# Parcourir les MR ouvertes
foreach ($mer in $openMers) {
    # Obtenir le nombre de commits Jira
    $jiraCommitCount = Get-JiraCommitCount -MerId $mer.Id -AccessToken $personalAccessToken

    # Vérifier le nombre de commits Jira
    if ($jiraCommitCount -lt $minimumJiraCommits) {
        # Bloquer la MR
        Block-GitLabMergeRequest -MerId $mer.Id -AccessToken $personalAccessToken -Message "MR bloquée : Nombre minimum de commits Jira requis non atteint ($minimumJiraCommits vs $jiraCommitCount)"

        # Envoyer une notification par e-mail
        Send-MailMessage -To $mer.AuthorEmail -From $fromEmailAddress -Subject "MR bloquée : $mer.Title" -Body "Bonjour $mer.AuthorName,

        Votre MR '$mer.Title' a été bloquée car elle ne respecte pas le nombre minimum de commits Jira requis. Le nombre minimum est de $minimumJiraCommits, mais la MR ne contient que $jiraCommitCount commits Jira.

        Veuillez ajouter des commits Jira pertinents à la MR et soumettre à nouveau la demande.

        Cordialement,
        Équipe GitLab" -SmtpServer $smtpServer -SmtpPort $smtpPort -SmtpUsername $smtpUsername -SmtpPassword $smtpPassword -UseSsl
    }
}
