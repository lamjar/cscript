#*****************************************************************************************************
# Script complet pour vérifier les commits GitLab et bloquer la MR avec notification Teams
# Ce script vous permet de vérifier le nombre de commits Jira dans les MR GitLab 
# et d'automatiser le blocage et la notification si nécessaire.
#*****************************************************************************************************
#                     .____                           __                 
#                     |    |    _____     _____      |__|_____  _______  
#                     |    |    \__  \   /     \     |  |\__  \ \_  __ \ 
#                     |    |___  / __ \_|  Y Y  \    |  | / __ \_|  | \/ 
#                     |_______ \(____  /|__|_|  //\__|  |(____  /|__|    
#                             \/     \/       \/ \______|     \/                                                           
#*****************************************************************************************************

# POUR LANCER CE SCRIPT 

#       @powershell -ExecutionPolicy Unrestricted -File check_mr_commits.ps1



# Remplacer par vos valeurs
$gitlabUrl = "https://gitlab.example.com"
$gitlabToken = "votre_jeton_d_accès"
$teamsWebhookUrl = "https://votre-webhook-teams.com"
$projects = "A", "B", "C"  # Noms des projets à surveiller

# Fonction pour envoyer une notification Teams
function Send-TeamsMessage($message) {
    Invoke-RestMethod -Method Post -Uri $teamsWebhookUrl -Headers @{Authorization="Content-Type=application/json"} -Body (ConvertTo-Json @{text=$message})
}

# Fonction pour vérifier et bloquer une MR
function Check-And-BlockMR($mrId) {
    # Obtenir les informations de la MR
    $mrInfo = Invoke-RestMethod -Method Get -Uri "$gitlabUrl/api/v4/merge_requests/$mrId" -Headers @{Authorization="Bearer $gitlabToken"}

    # Extraire l'ID du ticket Jira
    $jiraIssueId = (ConvertFrom-Json $mrInfo).issue_id

    # Vérifier si l'ID du ticket Jira est présent
    if (-eq $jiraIssueId $null) {
        Write-Error "Erreur : Impossible de trouver l'ID du ticket Jira pour la MR $mrId"
        return 1
    }

    # Obtenir la liste des commits de la MR
    $commits = Invoke-RestMethod -Method Get -Uri "$gitlabUrl/api/v4/merge_requests/$mrId/commits" -Headers @{Authorization="Bearer $gitlabToken"}

    # Compter le nombre de commits liés au ticket Jira
    $jiraCommitCount = (ConvertFrom-Json $commits).[] | Where-Object { $_.description -match "^\[$jiraIssueId\]" } | Measure-Object -Property Count | Select-Object -ExpandProperty Count

    # Vérifier si le nombre de commits dépasse deux
    if ($jiraCommitCount -gt 2) {
        # Envoyer une notification Teams
        Send-TeamsMessage "MR $mrId bloquée : Plus de 2 commits pour le ticket Jira $jiraIssueId"

        # Ajouter un label "bloqué" à la MR
        Invoke-RestMethod -Method Put -Uri "$gitlabUrl/api/v4/merge_requests/$mrId/labels" -Headers @{Authorization="Bearer $gitlabToken"} -Body (ConvertTo-Json @{labels=["bloqué"]})

        Write-Output "MR $mrId bloquée et label 'bloqué' ajouté"
        return 1
    }

    # La MR est conforme
    Write-Output "MR $mrId conforme : nombre de commits pour le ticket Jira $jiraIssueId est correct"
    return 0
}

# Parcourir les projets
foreach ($project in $projects) {
    # Obtenir la liste des MR du projet
    $mrs = Invoke-RestMethod -Method Get -Uri "$gitlabUrl/api/v4/projects/$project/merge_requests" -Headers @{Authorization="Bearer $gitlabToken"}

    # Parcourir les MR du projet
    foreach ($mrId in (ConvertFrom-Json $mrs).[].id) {
        Check-And-BlockMR $mrId
    }
}
