#*****************************************************************************************************
# Script complet pour vérifier les commits GitLab et bloquer la MR avec notification Teams
# Ce script vous permet de vérifier le nombre de commits Jira dans les MR GitLab 
# et d'automatiser le blocage et la notification si nécessaire.
#*****************************************************************************************************
#                     .____                           __                 
#                     |    |    _____     _____      |__|_____  _______  
#                     |    |    \__  \   /     \     |  |\__  \ \_  __ \ 
#                     |    |___  / __ \_|  Y Y  \    |  | / __ \_|  | \/ 
#                     |_______ \(____  /|__|_|  //\__|  |(____  /|__|    
#                             \/     \/       \/ \______|     \/                                                           
#*****************************************************************************************************

# POUR LANCER CE SCRIPT 

#       ./check_mr_commits.sh


#!/bin/bash

# Remplacer par vos valeurs
gitlab_url="https://gitlab.example.com"
gitlab_token="votre_jeton_d_accès"
teams_webhook_url="https://votre-webhook-teams.com"
projects=(A B C)  # Noms des projets à surveiller

# Fonction pour envoyer une notification Teams
send_teams_message() {
  message="$1"
  curl -X POST -H "Content-Type: application/json" "$teams_webhook_url" -d "{\"text\": \"$message\"}"
}

# Fonction pour vérifier et bloquer une MR
check_and_block_mr() {
  mr_id="$1"

  # Obtenir les informations de la MR
  mr_info=$(curl -H "Authorization: Bearer $gitlab_token" "$gitlab_url/api/v4/merge_requests/$mr_id")

  # Extraire l'ID du ticket Jira
  jira_issue_id=$(echo "$mr_info" | jq -r '.issue_id')

  # Vérifier si l'ID du ticket Jira est présent
  if [ -z "$jira_issue_id" ]; then
    echo "Erreur : Impossible de trouver l'ID du ticket Jira pour la MR $mr_id"
    return 1
  fi

  # Obtenir la liste des commits de la MR
  commits=$(curl -H "Authorization: Bearer $gitlab_token" "$gitlab_url/api/v4/merge_requests/$mr_id/commits")

  # Compter le nombre de commits liés au ticket Jira
  jira_commit_count=$(echo "$commits" | jq -r '.[] | select(.description | match("^\\[$jira_issue_id\\]")) | . | length')

  # Vérifier si le nombre de commits dépasse deux
  if [ $jira_commit_count -gt 2 ]; then
    # Envoyer une notification Teams
    send_teams_message "MR $mr_id bloquée : Plus de 2 commits pour le ticket Jira $jira_issue_id"

    # Ajouter un label "bloqué" à la MR
    curl -X PUT -H "Authorization: Bearer $gitlab_token" "$gitlab_url/api/v4/merge_requests/$mr_id/labels" -d "{\"labels\":[\"bloqué\"]}"

    echo "MR $mr_id bloquée et label 'bloqué' ajouté"
    return 1
  fi

  # La MR est conforme
  echo "MR $mr_id conforme : nombre de commits pour le ticket Jira $jira_issue_id est correct"
  return 0
}

# Parcourir les projets
for project in "${projects[@]}"; do
  # Obtenir la liste des MR du projet
  mrs=$(curl -H "Authorization: Bearer $gitlab_token" "$gitlab_url/api/v4/projects/$project/merge_requests")

  # Parcourir les MR du projet
  for mr_id in $(echo "$mrs" | jq -r '.[].id'); do
    check_and_block_mr $mr_id
  done
done
